# Install dependencies
Mac os X
```
brew install graphviz
pip install awscli --upgrade --user
```
# Install docker:
https://store.docker.com/editions/community/docker-ce-desktop-mac


# Install terraform (mac os X)
```
brew install terraform
```
# The secrets.tfvars file format is: (edit and replaec with your secrets)

Create a file secrets.tfvars with the following content replaced:

access_key 	= "replaceme"
secret_key 	= "replaceme"
key_name 	  = "replaceme"
region 		  = "replaceme"
profile     = "replace me if multiple aws profiles exist on the machine or just default"

# Description
This project contains 3 main terraform modules:
1. core will build all the AWS resources - VPC, multiAZ, Internet gateway ...
2. ECS will configure:
  - the ecs cluster needed to run the any app with autoscalling groups (2/3 instances running at all times)
  - ecr registry
  - setup IAM roles
3. This will build locally using docker (ensure your local machine can build a docker file) and push the demo app onto ECR and then will run it as a Load Balanced service exposed over the internet on port 80.

*Note:* See ELB for the internet facing hostname and point your browser to http://elb_address once the app is deployed. There is a minute or so delay between the moment the containers are started and the ELB registers the service healthy state.

# How to deploy the demo_app
This will get all the modules, install all the plugins and apply all the infrastructure.
```
terraform get
terraform init
terraform plan -var-file="secrets.tfvars"
terraform graph |dot -Tpng -oPlan_graph.png
terraform apply -var-file="secrets.tfvars"
```
# To destroy run bellow
```
terraform destroy -var-file="secrets.tfvars"
```

# Local development:

Due to the fact that the defined in demo_app/app folder and contains a Dockerfile local development can follow best practices and run the app on the localhost using docker.

Commands like this can support developers:
```
cd demo_app
docker build -t demo-app -f demo_app/Dockerfile .
docker run -p 127.0.0.1:8080:8080 demo-app
```

Point your browser at http://localhost:8080 and your app will load.
Improvements can be added by supplying a docker compose file or integrate it via a CI Pipeline.

# Troubleshooting and advanced usage:
### Terraform plan modules independently:
```
terraform plan -var-file="secrets.tfvars" -target=module.core
terraform plan -var-file="secrets.tfvars" -target=module.ecs
terraform plan -var-file="secrets.tfvars" -target=module.demo_app
```
### Improvements to consider:
https://github.com/diosmosis/terraform-provider-docker-image
