/*Outputs variables to be used by other modules (example instances)*/

output "vpc_id_core" {
  value = "${aws_vpc.demo_main_vpc.id}"
}

output "subnet_id_demo" {
  value = "${aws_subnet.eu-west-2a-public.id}"
}

output "demo_instance_profile_core" {
  value = "${aws_iam_instance_profile.demo_instance_profile.id}"
}
