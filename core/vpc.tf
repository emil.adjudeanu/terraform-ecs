/*Define VPC*/

resource "aws_vpc" "demo_main_vpc" {
  cidr_block       = "10.2.0.0/16"
  instance_tenancy = "default"
  enable_dns_hostnames = true

  tags {
    Name = "demo_main_vpc"
  }
}

/*Public Subnets*/

resource "aws_subnet" "eu-west-2a-public" {
  vpc_id = "${aws_vpc.demo_main_vpc.id}"

  cidr_block = "${var.public_subnet_cidr-a}"
  availability_zone = "eu-west-2a"

  tags {
      Name = "Demo Public Subnet A"
  }
}
resource "aws_subnet" "eu-west-2b-public" {
  vpc_id = "${aws_vpc.demo_main_vpc.id}"

  cidr_block = "${var.public_subnet_cidr-b}"
  availability_zone = "eu-west-2b"

  tags {
      Name = "Demo Public Subnet B"
  }
}
resource "aws_subnet" "eu-west-2c-public" {
  vpc_id = "${aws_vpc.demo_main_vpc.id}"

  cidr_block = "${var.public_subnet_cidr-c}"
  availability_zone = "eu-west-2c"

  tags {
      Name = "Demo Public Subnet C"
  }
}

/*Define internet gateway*/

resource "aws_internet_gateway" "demo_main_gw" {
  vpc_id = "${aws_vpc.demo_main_vpc.id}"

  tags {
    Name = "demo main internet gateway"
  }
}

/*Define routing table for internet access*/

resource "aws_route_table" "eu-west-2a-public" {
  vpc_id = "${aws_vpc.demo_main_vpc.id}"

  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = "${aws_internet_gateway.demo_main_gw.id}"
  }

  tags {
      Name = "Public Subnet A default internet route"
  }
}

resource "aws_route_table" "eu-west-2b-public" {
  vpc_id = "${aws_vpc.demo_main_vpc.id}"

  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = "${aws_internet_gateway.demo_main_gw.id}"
  }

  tags {
      Name = "Public Subnet B default internet route"
  }
}

resource "aws_route_table" "eu-west-2c-public" {
  vpc_id = "${aws_vpc.demo_main_vpc.id}"

  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = "${aws_internet_gateway.demo_main_gw.id}"
  }

  tags {
      Name = "Public Subnet C default internet route"
  }
}

/*A a new route in the existing VPC routing table*/

/*resource "aws_route" "eu-west-1a-public" {
  route_table_id            = "${aws_route.demo_main_vpc.id}"
  destination_cidr_block    = "0.0.0.0/0"
  depends_on                = ["aws_route_table.testing"]
}*/

/*Map routing table for internet access*/

resource "aws_route_table_association" "eu-west-2a-public" {
  subnet_id = "${aws_subnet.eu-west-2a-public.id}"
  route_table_id = "${aws_route_table.eu-west-2a-public.id}"
}

resource "aws_route_table_association" "eu-west-2b-public" {
  subnet_id = "${aws_subnet.eu-west-2b-public.id}"
  route_table_id = "${aws_route_table.eu-west-2b-public.id}"
}

resource "aws_route_table_association" "eu-west-2c-public" {
  subnet_id = "${aws_subnet.eu-west-2c-public.id}"
  route_table_id = "${aws_route_table.eu-west-2c-public.id}"
}
