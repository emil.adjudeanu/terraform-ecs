/* ELB for the registry */
resource "aws_elb" "demo-app-elb" {
  name               = "demo-app-elb"
  availability_zones = ["${split(",", var.availability_zones)}"]

  listener {
    instance_port     = 8080
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  /* @todo - handle SSL */
  /*listener {
    instance_port = 5000
    instance_protocol = "http"
    lb_port = 443
    lb_protocol = "https"
    ssl_certificate_id = "arn:aws:iam::123456789012:server-certificate/certName"
  }*/

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:8080/"
    interval            = 30
  }
  cross_zone_load_balancing = true
  connection_draining = false

  tags {
    Name = "demo-app-elb"
  }
}

/* TO DO Build the docker image locally and push to registry */
resource "null_resource" "build_docker_image" {
  provisioner "local-exec" {
    command = "docker build -t demo-app -f demo_app/Dockerfile ."
  }
}
resource "null_resource" "get-ecs-login" {
  provisioner "local-exec" {
    command = "$(aws --profile ${var.profile} --region ${var.region} ecr get-login --no-include-email|sed 's|https://||')"
  }
}

resource "null_resource" "docker-tag-image" {
  provisioner "local-exec" {
    command = "docker tag demo-app:latest ${var.ecr_repository_url}:latest"
  }
  /*It seems that only local resources are supported - see issues 10462*/
  depends_on = ["aws_ecs_task_definition.demo-app"]
}

resource "null_resource" "docker-push-image" {
  provisioner "local-exec" {
    command = "docker push ${var.ecr_repository_url}:latest"
  }
  /*It seems that only local resources are supported - see issues 10462*/
  depends_on = ["aws_ecs_task_definition.demo-app"]
}

/* container and task definitions for running the actual Docker demo app */
resource "aws_ecs_service" "demo-app-service" {
  name            = "demo-app-service"
  cluster         = "${var.ecs_cluster_name}"
  task_definition = "${aws_ecs_task_definition.demo-app.arn}"
  desired_count   = 3
  depends_on = ["aws_ecs_task_definition.demo-app"]

  load_balancer {
    elb_name       = "${aws_elb.demo-app-elb.id}"
    container_name = "demo-app"
    container_port = 8080
  }
}

data "template_file" "ecs_task_definition_template_file" {
  template = "${file("demo_app/task-definitions/demo-app.json")}"

  vars {
    registry_docker_image = "${var.ecr_repository_url}:latest"
  }
}

resource "aws_ecs_task_definition" "demo-app" {
  family                = "demo-app"
  container_definitions = "${data.template_file.ecs_task_definition_template_file.rendered}"
}
/*Doesn't work as expected*/
output "demo_app_elb_dns_name" {
  value = "${aws_elb.demo-app-elb.dns_name}"
}
