#

variable "region" {}
variable "availability_zones" {}
variable "key_name" {}
variable "rsa_key" {}
variable "ecs_cluster_name" {}
variable "ecr_name" {}
variable "ecr_repository_url" {}
variable "profile" {
  default = "default"
}
