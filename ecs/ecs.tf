
/**
 * Launch configuration used by autoscaling group
 */
resource "aws_launch_configuration" "ecs" {
  name                   = "ecs"
  image_id               = "${lookup(var.amis, var.region)}"
  instance_type          = "${var.instance_type}"
  key_name               = "${var.key_name}"
  iam_instance_profile   = "${aws_iam_instance_profile.ecs.id}"
  security_groups        = ["${aws_security_group.ecs.id}"]
  iam_instance_profile   = "${aws_iam_instance_profile.ecs.name}"
  user_data              = "#!/bin/bash\necho ECS_CLUSTER=${aws_ecs_cluster.emil-ecs-cluster.name} > /etc/ecs/ecs.config"
}

/**
 * Autoscaling group.
 */
resource "aws_autoscaling_group" "ecs" {
  name                 = "ecs-asg"
  availability_zones   = ["${split(",", var.availability_zones)}"]
  launch_configuration = "${aws_launch_configuration.ecs.name}"
  /* @todo - variablize */
  min_size             = 2
  max_size             = 3
  desired_capacity     = 2
}

/* ecs service cluster */
resource "aws_ecs_cluster" "emil-ecs-cluster" {
  name = "${var.ecs_cluster_name}"
}
