/*Outputs variables to be used by other modules ex: demo_app*/

output "ecr_repository_url" {
  value = "${aws_ecr_repository.demo-repository.repository_url}"
}
