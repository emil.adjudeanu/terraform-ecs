#
# From other modules
#
/*TO DO: define defaults if possible*/

variable "region" {}
variable "availability_zones" {}
variable "key_name" {}
variable "rsa_key" {}
variable "ecs_cluster_name" {}
variable "ecr_name" {}
variable "profile" {}


variable "instance_type" {
  default = "t2.micro"
}

/* ECS optimized AMIs per region */
variable "amis" {
  default = {
    ap-northeast-1 = "ami-8aa61c8a"
    ap-southeast-2 = "ami-5ddc9f67"
    eu-west-1      = "ami-2aaef35d"
    eu-west-2      = "ami-f4e20693"
    us-east-1      = "ami-b540eade"
    us-west-1      = "ami-5721df13"
    us-west-2      = "ami-cb584dfb"
  }
}
