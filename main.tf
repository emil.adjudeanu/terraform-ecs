provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
}

/* SSH key pair */
resource "aws_key_pair" "demo_deployer" {
  key_name   = "${var.key_name}"
  public_key = "${var.rsa_key}"
}

module "core" {
  source = "./core"
  public_subnet_cidr-a = "${var.public_subnet_cidr-a}"
  public_subnet_cidr-b = "${var.public_subnet_cidr-b}"
  public_subnet_cidr-c = "${var.public_subnet_cidr-c}"
}

module "ecs" {
  source    = "./ecs"
  region    = "${var.region}"
  profile   = "${var.profile}"
  availability_zones = "${var.availability_zones}"
  key_name  = "${var.key_name}"
  rsa_key   = "${var.rsa_key}"
  ecs_cluster_name = "${var.ecs_cluster_name}"
  ecr_name  = "${var.ecr_name}"
}

module "demo_app" {
  source    = "./demo_app"
  region    = "${var.region}"
  profile   = "${var.profile}"
  ecr_name   = "${var.ecr_name}"
  availability_zones = "${var.availability_zones}"
  key_name  = "${var.key_name}"
  rsa_key   = "${var.rsa_key}"
  ecs_cluster_name = "${var.ecs_cluster_name}"
  ecr_repository_url = "${module.ecs.ecr_repository_url}"
}
