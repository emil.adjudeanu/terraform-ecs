/*Define Variables*/

variable "public_subnet_cidr-a" {
    description = "CIDR for the Demo Public Subnet"
    default = "10.2.1.0/24"
}
variable "public_subnet_cidr-b" {
    description = "CIDR for the Demo Public Subnet"
    default = "10.2.2.0/24"
}
variable "public_subnet_cidr-c" {
    description = "CIDR for the Demo Public Subnet"
    default = "10.2.3.0/24"
}

variable "access_key" {}
variable "secret_key" {}

variable "region" {
  default = "eu-west-2"
}

variable "profile" {
  default = "default"
}

variable "availability_zones" {
  description = "The availability zones"
  default = "eu-west-2a,eu-west-2b,eu-west-2c"
}

variable "key_name" {
  description = "The aws ssh key name."
  default = "deployer-key"
}

variable "rsa_key" {
  description = "The ssh public key for using with the cloud provider."
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDEVcrW5OgVWNC/jHV9ppM191W8GkHqXybxLYfynGHjKvklMLw/PbGhQKxfj3YI08jRbJ9gwPBCg7mLLYRDr/+0G5jfnqJ6HbRjkZFUw7WLcDS6dpx1KhnQctZBoD3M2Hz7FCPrC2p0jYmJeZ1bFP/WGM+cLDGrDiCVTbft34PUo5CuMgkxiIaJurKWd5jE9sfaYu9C+uOLyD7iSPNlF7iJBDIzi70ywNSF4mBS1acVFrwF3oQT3tUELQidf9mapyEAw0ezTc9sGQZw9cPvhFqtxMBUSLu28Mp/3csjRdsyA+8UALMIb7Hcx5GiLijmIRMk0kQJ/jWafJ4D6mOLIqKV3KQoMtF9z5mlzEDq27I7H0yAUhoMn9jMEfEkLx/Yst+CBUoyWbY/8648S+JBZAIYNo+n5rOGSRoT2i3uD+/tdL90buJmflHmrxAHZGMhsztDOeeJMQzlidw/YjRPP9biwVicwba0s9JHkHug2/FDY62ZUTzC8mf/6GqRi3lxOaaay5ZjhMHPlgwDhtj1tFcai49MIWlFGoZNZxv/Yglm4KmRu71ph6cuLOtbXncLTkmSqCzT7XxKZwkXD4wSYoueQMOFJUgLxja7HUhM02TJ8Ba0f5X0JHEs6NRY4TQAQ48eLa4S3qZ4A55h8dP8xgNnXu16ZJidJ4JMrLOD0ya27w== emil@emil-laptop"
}

variable "ecs_cluster_name" {
  description = "The name of the Amazon ECS cluster."
  default = "demo-cluster"
}

variable "ecr_name" {
  description = "The name of the Amazon ECR repo."
  default = "demo-app"
}
